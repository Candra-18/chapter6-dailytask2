const { Elektronik } = require("../models");

module.exports = {
  getAllelektronik(a) {
    const elektroniks = Elektronik.findAll(a);
    return elektroniks;
  },
  create(id_user, name, color, price) {
    return Elektronik.create({ id_user, name, color, price });
  },

  updateelektronik(selectelektronik, newelektronik) {
    return selectelektronik.update(newelektronik);
  },

  setelektronik(selectelektronik) {
    return Elektronik.findByPk(selectelektronik);
  },

  destroyelektronik(selectelektronik) {
    return selectelektronik.destroy();
  },

};
