const { user } = require("../models");

module.exports = {
  getAllusers(a) {
    const  users = user.findAll(a);
    return  users;
  },
  create(username, email,  password) {
    return user.create({ username, email,  password});
  },
  updateusers(selectuser, newuser) {
    return selectuser.update(newuser);
  },

  setUsers(selectUser) {
    return user.findByPk(selectUser);
  },

  destroyUsers(selectUser) {
    return selectUser.destroy();
  },

};
