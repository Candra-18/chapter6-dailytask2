'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Elektronik extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
        //association can be defined here
        Elektronik.belongsTo(models.user, {
          foreignKey: 'id_user'
        });
      };
  }
  Elektronik.init({
    id_user: DataTypes.INTEGER,
    name: DataTypes.STRING,
    color: DataTypes.STRING,
    price: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Elektronik',
  });
  return Elektronik;
};