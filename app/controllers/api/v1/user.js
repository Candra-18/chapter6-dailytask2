/**
 * @file contains request handler of post resource
 * @author Fikri Rahmat Nurhidayat
 */
 const { Elektronik,user  } = require("../../../models");
 const usersService = require("../../../services/user");
 
 module.exports = {
   list(req, res) {
    usersService
       .getAllusers({
         include:{
           model:Elektronik
         }
       })
       .then((users) => {
         res.status(200).json({
           status: "OK",
           data: {
             users,
           },
         });
       })
       .catch((err) => {
         res.status(400).json({
           status: "FAIL",
           message: err.message,
         });
       });
   },
 
   create(req, res) {
    const {username, email, password } = req.body;
    usersService
      .create(username, email, password)
      .then((users) => {
        res.status(201).json({
          status: "OK",
          data: users,
        });
      })
      .catch((err) => {
        res.status(201).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },
 
   update(req, res) {
     const users = req.users;
     usersService
       .updateusers(users, req.body)
       .then(() => {
         res.status(200).json({
           status: "OK",
           data: users,
         });
       })
       .catch((err) => {
         res.status(422).json({
           status: "FAIL",
           message: err.message,
         });
       });
   },
 
   show(req, res) {
     const user = req.users;
 
     res.status(200).json({
       status: "OK",
       data: user,
     });
   },
 
   destroy(req, res) {
    usersService
       .destroyUsers(req.users)
       .then(() => {
         res.status(204).end();
       })
       .catch((err) => {
         res.status(422).json({
           status: "FAIL",
           message: err.message,
         });
       });
   },
 
   setUsers(req, res, next) {
    usersService
       .setUsers(req.params.id)
       .then((users) => {
         if (!users) {
           res.status(404).json({
             status: "FAIL",
             message: "Post not found!",
           });
 
           return;
         }
 
         req.users = users;
         next();
       })
       .catch((err) => {
         res.status(404).json({
           status: "FAIL",
           message: "Post not found!",
         });
       });
   },
 };
 