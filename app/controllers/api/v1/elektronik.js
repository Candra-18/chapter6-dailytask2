/**
 * @file contains request handler of post resource
 * @author Fikri Rahmat Nurhidayat
 */
 const { Elektronik,user } = require("../../../models");
const elektronikService = require("../../../services/elektronik");

module.exports = {
  list(req, res) {
    elektronikService
      .getAllelektronik({
        include:{
          model:user
        }
      })
      .then((elektroniks) => {
        res.status(200).json({
          status: "OK",
          data: {
            elektroniks,
          },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  create(req, res) {
    const { id_user, name, color, price } = req.body;
    elektronikService
      .create(id_user, name, color, price)
      .then((Elektroniks) => {
        res.status(201).json({
          status: "OK",
          data: Elektroniks,
        });
      })
      .catch((err) => {
        res.status(201).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  update(req, res) {
    const elektroniks = req.elektronik;
    elektronikService
      .updateelektronik(elektroniks, req.body)
      .then(() => {
        res.status(200).json({
          status: "OK",
          data: elektroniks,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    const elektroniks = req.elektronik;

    res.status(200).json({
      status: "OK",
      data: elektroniks,
    });
  },

  destroy(req, res) {
    elektronikService
      .destroyelektronik(req.elektronik)
      .then(() => {
        res.status(204).end();
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  setelektronik(req, res, next) {
    elektronikService
      .setelektronik(req.params.id)
      .then((elektroniks) => {
        if (!elektroniks) {
          res.status(404).json({
            status: "FAIL",
            message: "elektronik not found!",
          });

          return;
        }

        req.elektronik = elektroniks;
        next();
      })
      .catch((err) => {
        res.status(404).json({
          status: "FAIL",
          message: "elektronik not found!",
        });
      });
  },
};
