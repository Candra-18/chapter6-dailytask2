/**
 * @file contains entry point of controllers api v1 module
 * @author Fikri Rahmat Nurhidayat
 */

const post = require("./post");
const user = require("./user");
const elektronik = require("./elektronik");

module.exports = {
  post,
  user,
  elektronik
};
