'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('users', [{
      "id": 1,
      "username": "Ikhsan",
      "email": "Ikhsan@gmail.com",
      "password": "123",
      createdAt: new Date(),
      updatedAt: new Date()
 
    }])

    await queryInterface.bulkInsert('Elektroniks', [{
      "id": 1,
      "id_user": 1,
      "name": "Laptop",
      "color": " Merah",
      "price": 123445,
      createdAt: new Date(),
      updatedAt: new Date()
 
    }])

  },

  async down (queryInterface, Sequelize) {
 await queryInterface.bulkDelete('users', null, {});
 await queryInterface.bulkDelete('Elektroniks', null, {});
 
  }
};
